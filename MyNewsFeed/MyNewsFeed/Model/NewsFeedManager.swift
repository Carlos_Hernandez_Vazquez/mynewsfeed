//
//  NewsFeedManager.swift
//  MyNewsFeed
//
//  Created by Carlos Hernández Vázquez on 24/03/21.
//
// Optional keys
//b4bc45efd37e40f58290660512dd3f7e
//6c28ad1e55f8438f9e4ae7eeb02652f9

import Foundation

typealias onError = (String) -> Void
typealias onSuccess = (Result) -> Void

struct NewsFeedManager {
    
    private let URL_BASE = "https://newsapi.org/v2"
    private let URL_GET_EVERYTHING = "/everything"
    private let URL_GET_HEADLINES = "/top-headlines"
    private let API_KEY = "064c01abcd3a4258876184243d5eb382"
    private let URL_SESSION = URLSession(configuration: .default)
    public var urlString : String?
    
    
    mutating func fetchNews (country: String?, category : String, date : String?, language : String, countryName : String?) {
        
        if let selectedDate = date {
            
            if let countryName = countryName {
                
                urlString = "\(URL_BASE)\(URL_GET_EVERYTHING)?q=\(countryName)&from=\(selectedDate)&to=\(selectedDate)&language=\(language)&apiKey=\(API_KEY)"
                            print(urlString ?? "No URL")
            }
            
        } else if let currentCountry = country {
            
            print("2")
            let countryId = currentCountry.lowercased()
            
            urlString = "\(URL_BASE)\(URL_GET_HEADLINES)?country=\(countryId)&category=\(category)&language=\(language)&apiKey=\(API_KEY)"
            debugPrint(urlString ?? "No URL")

            
        } else {
            
            print("3")
            urlString = "\(URL_BASE)\(URL_GET_HEADLINES)?category=\(category)&language=\(language)&apiKey=\(API_KEY)"
            debugPrint(urlString ?? "No URL")
            
        }
        
    }
    
    func performRequest (onSucces : @escaping onSuccess, onError : @escaping onError) {
        
        let url = URL(string: (urlString?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!)!
        
        let task = URL_SESSION.dataTask(with: url) { (data, response, error) in
            
            DispatchQueue.main.async {
                
                if let error = error {
                    
                    onError(error.localizedDescription)
                    
                }
                
                guard let data = data, let response = response as? HTTPURLResponse else {
                    
                    onError("No data")
                    
                    return
                    
                }
                
                do {
                    
                    if response.statusCode == 200 {
                        
                        let result = try JSONDecoder().decode(Result.self, from: data)
                        
                        onSucces(result)
                        
                    } else {
                        
                        let error = try JSONDecoder().decode(APIError.self, from: data)
                        
                        print("Error \(error.code). \(error.message)")
                        onError("Error \(error.code). \(error.message)")
                        
                    }
                    
                    
                } catch {
                    
                    onError(error.localizedDescription)
                    
                }
                
                
            }
            
            
        }
        
        task.resume()
        
    }
    
    
}
