//
//  APIError.swift
//  MyNewsFeed
//
//  Created by Carlos Hernández Vázquez on 24/03/21.
//

import Foundation

struct APIError : Codable {

    public let code : String
    public let message : String
    
}
