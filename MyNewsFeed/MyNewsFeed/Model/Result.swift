//
//  Results.swift
//  MyNewsFeed
//
//  Created by Carlos Hernández Vázquez on 24/03/21.
//

import Foundation

struct Result : Codable {
    
    public let articles : [Article]
    
}

struct Article : Codable {
    
    public let source : Source
    public let title : String
    public let publishedAt : String
    public let url : String
    
    
}

struct Source : Codable {
    
    public let name : String
    
}
