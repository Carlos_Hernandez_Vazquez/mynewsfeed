//
//  K.swift
//  MyNewsFeed
//
//  Created by mac on 23/03/21.
//

import Foundation

struct K {
    
    
    static let articleCell = "ArticleCell"
    static let categories = ["general", "business", "technology", "sports"]
    
    struct SegueIdentifiers {
        
        static let firstToLocalization = "FirstToLocalization"
        static let locationToFeed = "LocationToFeed"
        static let newsFeedToNewDetail = "NewsFeedVCToNewDetailVC"
        
    }
    
}
