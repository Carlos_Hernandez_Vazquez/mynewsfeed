//
//  ArticleCell.swift
//  MyNewsFeed
//
//  Created by Carlos Hernández Vázquez on 24/03/21.
//

import UIKit

class ArticleCell: UITableViewCell {

    //MARK: - Outlets
    
    @IBOutlet weak var sourceAndTimeLabel: UILabel!
    @IBOutlet weak var newsTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell (with article : Article) {
        
        var pusblishDate = article.publishedAt
        
        sourceAndTimeLabel.text = "\(article.source.name) - \(pusblishDate.showOnlyTime())"
        newsTitleLabel.text = "\(article.title)"
        
    }
    
    
    static func nibName () -> UINib {
        
        let nibName = UINib(nibName: K.articleCell, bundle: nil)
        
        return nibName
        
    }
    
}
