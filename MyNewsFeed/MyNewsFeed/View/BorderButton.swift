//
//  BorderButton.swift
//  MyNewsFeed
//
//  Created by Carlos Hernández Vázquez on 23/03/21.
//

import UIKit

class BorderButton: UIButton {

    override func awakeFromNib() {
        
        layer.borderWidth = 2.0
        layer.borderColor = UIColor.black.cgColor
        
    }

}
