//
//  LocalizationVC.swift
//  MyNewsFeed
//
//  Created by mac on 23/03/21.
//

import UIKit
import CoreLocation

class LocalizationVC: UIViewController {

    
    private let locationManager = CLLocationManager()
    private var countryName : String?
    private var countryCode : String?
    private let geoCoder = CLGeocoder()
    
    //MARK: - Lyfe Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.hidesBackButton = true
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        
        
    }
    
    //MARK: - LocalizationVC Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let myNewsFeedVC = segue.destination as? MyNewsFeedVC else { return }
        
        myNewsFeedVC.initCountryName(with: self.countryName, code: countryCode)
                
    }
    

}

//MARK: - CLLocationManagerDelegate Methods

extension LocalizationVC : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let currentLocation = locations.first else { return }

        geoCoder.reverseGeocodeLocation(currentLocation) { (placemarks, error) in
        
            guard let currentLocPlacemark = placemarks?.first else { return }
                    
            self.countryName = currentLocPlacemark.country
            self.countryCode = currentLocPlacemark.isoCountryCode
            print(currentLocPlacemark.country ?? "")
            print(currentLocPlacemark.isoCountryCode ?? "")
            self.performSegue(withIdentifier: K.SegueIdentifiers.locationToFeed, sender: self)
                    
        }
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print(error.localizedDescription)
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedWhenInUse {
            self.performSegue(withIdentifier: K.SegueIdentifiers.locationToFeed, sender: self)
            
            
        } else if status == .denied {
            
            let alertLocation = UIAlertController(title: "Error", message: "You cannot continue if you do not accept location permissions for My News Feed", preferredStyle: .alert)
            let actionAllowAccess = UIAlertAction(title: "Allow access to the location", style: .default) { (action) in
                
                if let url = URL(string:UIApplication.openSettingsURLString) {
                    
                    if UIApplication.shared.canOpenURL(url) {
                        
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        
                    }
                }
                
            }
            
            
            alertLocation.addAction(actionAllowAccess)
            
            present(alertLocation, animated: true, completion: nil)
            
            
        }
        
        
    }
    
    
}
