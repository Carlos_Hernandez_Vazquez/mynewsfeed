//
//  SignUpVC.swift
//  MyNewsFeed
//
//  Created by Carlos Hernández Vázquez on 23/03/21.
//

import UIKit
import Firebase

class SignUpVC: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordConfirmationTextField: UITextField!
    
    //MARK: - Lyfe Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        passwordConfirmationTextField.delegate = self
        passwordTextField.textContentType = .oneTimeCode
        passwordConfirmationTextField.textContentType = .oneTimeCode
        
        
    }
    
    
    //MARK: - Actions
    
    @IBAction func signUpButtonWasPressed(_ sender: BorderButton) {
        
        
        if let email = emailTextField.text, let password = passwordTextField.text, let confirmationPassword = passwordConfirmationTextField.text  {
        
            if password == confirmationPassword {
        
                Auth.auth().createUser(withEmail: email, password: password) { (auth, error) in
                    
                    if let e = error {
                        
                        let errorAlert = UIAlertController(title: "Error", message: e.localizedDescription, preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                        
                        errorAlert.addAction(action)
                        
                        self.present(errorAlert, animated: true, completion: nil)
                    
                    } else {
                        
                        let successAction = UIAlertController(title: "Registered", message: "User has been successfully registered", preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default) { (action) in
                            
                            self.navigationController?.popToRootViewController(animated: true)
                            
                        }
                        
                        successAction.addAction(action)
                        self.present(successAction, animated: true, completion: nil)
                        
                    }
                    
                    
                    
                }

            
            } else {
            
                let alert = UIAlertController(title: "Error", message: "Passwords do not match. Try again", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            
                alert.addAction(action)
            
                present(alert, animated: true, completion: nil)
            
            }
        
        }
        
        
        
    }
    
    //MARK: - SignUpVC Methods

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
}

extension SignUpVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    
}
