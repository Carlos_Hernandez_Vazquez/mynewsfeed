//
//  MyNewsFeed.swift
//  MyNewsFeed
//
//  Created by mac on 24/03/21.
//

import UIKit
import Firebase

class MyNewsFeedVC: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var articlesTableView: UITableView!
    @IBOutlet weak var locationSegmentedControl: UISegmentedControl!
    @IBOutlet weak var categorySegmentedControl: UISegmentedControl!
    
    
    private let datePicker = UIDatePicker()
    private var newsFeedManager = NewsFeedManager()
    private var articles = [Article]()
    private var category = "general"
    public var coutryName : String?
    private var currentCountryCode : String?
    
    
    private var language : String {
      
        let preferredLanguage = String(NSLocale.preferredLanguages[0])
        return preferredLanguage.dropLast(3).lowercased()
        
    }
        
        
    //MARK: - Lyfe Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addPickerView()
        addLogOutButton()
        navigationItem.hidesBackButton = true
        newsFeedManager.fetchNews(country: currentCountryCode, category: "general", date: nil, language: language, countryName: nil)
        updateView()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        articlesTableView.delegate = self
        articlesTableView.dataSource = self
        articlesTableView.register(ArticleCell.nibName(), forCellReuseIdentifier: K.articleCell)
        
    }
    
    //MARK: - Actions
    
    @IBAction func getNewsWithDateButtonWasPressed(_ sender: UIButton) {
        
        if dateTextField.text != "" {
            
            
            
            if let date = dateTextField.text {
                
                coutryName = nil
                
                if let currentCountryName = coutryName {
                    newsFeedManager.fetchNews(country: "us", category: "general", date: date, language: language, countryName: currentCountryName)
                    updateView()

                } else {
                    
                    
                    let alert = UIAlertController(title: "Error", message: "Your country was not found", preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    
                    alert.addAction(action)
                    
                    present(alert, animated: true, completion: nil)
                    
                    
                }
                
                
                
                                
            }
            
            
        } else {
            
            let alert = UIAlertController(title: "Error", message: "You have not entered any date", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
            
        }
        
        
    }
    
    
    @IBAction func locationSegmentedControl(_ sender: UISegmentedControl) {
        
        
        if sender.selectedSegmentIndex == 0 {

            
            category = K.categories[categorySegmentedControl.selectedSegmentIndex]
            newsFeedManager.fetchNews(country: currentCountryCode, category: category, date: nil, language: language, countryName: nil)
            updateView()
            
            
        } else {
            
            category = K.categories[categorySegmentedControl.selectedSegmentIndex]
            newsFeedManager.fetchNews(country: nil, category: category, date: nil, language: language, countryName: nil)
            updateView()
            
        }
        
    }
    
    
    @IBAction func categoriesSegmentedControl(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        
        case 0:
            
            category = K.categories[categorySegmentedControl.selectedSegmentIndex]
            print(category)
            performRequestbyLocation()
            
            
        case 1:
            
            category = K.categories[categorySegmentedControl.selectedSegmentIndex]
            print(category)
            performRequestbyLocation()

        case 2:
            
            category = K.categories[categorySegmentedControl.selectedSegmentIndex]
            print(category)
            performRequestbyLocation()

        default:
            
            category = K.categories[categorySegmentedControl.selectedSegmentIndex]
            print(category)
            performRequestbyLocation()
            
        }
        
    }
    
    

    //MARK: - MyNewsFeedVC Methods
    
    private func addLogOutButton () {
        
        let logOut = UIAction(title: "Log Out", image: nil, identifier: nil, discoverabilityTitle: nil, attributes: .init(), state: .off) { (action) in
            
            do {
                
                try Auth.auth().signOut()
                debugPrint("Success")
                self.navigationController?.popToRootViewController(animated: true)
                
                
            } catch let signOutError as NSError {
                
                print("Error signing out : %@", signOutError)
                
            }
            
            
        }
        
        let actions = [logOut]
        
        let menu = UIMenu(title: "", image: nil, identifier: nil, options: .displayInline, children: actions)
        
        let logOutButton = UIBarButtonItem(title: "Log ", image: UIImage(systemName: "person.fill")!, primaryAction: nil, menu: menu)
        
        self.navigationItem.rightBarButtonItem = .init(logOutButton)
        self.navigationItem.rightBarButtonItem = logOutButton
        self.navigationItem.rightBarButtonItem?.menu = menu
        
    }
    
    public func initCountryName(with country : String?, code : String?) {
        
        if let currentCountryName = country, let countryCode = code {
        
            self.coutryName = currentCountryName
            self.currentCountryCode = countryCode.lowercased()

            
        }
        
                
    }
    
    private func addPickerView () {
        
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        
        let tooolBar = UIToolbar()
        
        tooolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(getDate))
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(endEditing))
        
        tooolBar.setItems([doneButton, cancelButton], animated: true)
    
        dateTextField.inputAccessoryView = tooolBar
        dateTextField.inputView = datePicker
        
    }
    
    @objc func getDate () {
        
        dateTextField.text = datePicker.date.addformat()
        self.view.endEditing(true)
        
        
    }
    
    @objc func endEditing () {
        
        
        self.view.endEditing(true)
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    private func updateView () {
        
        newsFeedManager.performRequest { (result) in
            
            self.articles = result.articles
            self.articlesTableView.reloadData()
            
            if self.articles.count != 0 {
            
                let indexPath = IndexPath(row: 0, section: 0)
                self.articlesTableView.scrollToRow(at: indexPath, at: .top, animated: true)

            }
            
                        
        } onError: { (error) in
            
            let errorAlert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            errorAlert.addAction(action)
            
            self.present(errorAlert, animated: true, completion: nil)
            
        }

        
    }
    
    private func performRequestbyLocation () {
        
        if locationSegmentedControl.selectedSegmentIndex == 0  {
            
            
            newsFeedManager.fetchNews(country: currentCountryCode, category: category, date: nil, language: language, countryName: nil)
            updateView()
            
        } else {
            
            newsFeedManager.fetchNews(country: nil, category: category, date: nil, language: language, countryName: nil)
            updateView()
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let newDetailVC = segue.destination as? NewDetailVC else { return }
        
        assert(sender as? String != nil)
        
        newDetailVC.getURL(url: sender as! String)
        
        
    }

}

//MARK: - UITableViewDelegate Methods

extension MyNewsFeedVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 150
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let url = self.articles[indexPath.row].url
        
        performSegue(withIdentifier: K.SegueIdentifiers.newsFeedToNewDetail, sender: url)
        
    }
    
    
}

//MARK: - UITableViewDataSource Methods

extension MyNewsFeedVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return articles.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        
        let cell = articlesTableView.dequeueReusableCell(withIdentifier: K.articleCell, for: indexPath) as? ArticleCell
        
        let article = articles[indexPath.row]
        
        cell?.configureCell(with: article)
        
        return cell!
        
    }
    
    
    
    
}
