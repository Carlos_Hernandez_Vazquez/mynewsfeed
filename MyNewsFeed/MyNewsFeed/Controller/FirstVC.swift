//
//  FirstVC.swift
//  MyNewsFeed
//
//  Created by Carlos Hernández Vázquez on 23/03/21.
//

import UIKit
import Firebase

class FirstVC: UIViewController {
    
    
    //MARK: - Outlets
    
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    public let defaults = UserDefaults.standard
    
    //MARK: - Lyfe Cycle
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getLasUserEmail()
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.tintColor = .black
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        overrideUserInterfaceStyle = .light
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        userTextField.delegate = self
        passwordTextField.delegate = self
        passwordTextField.textContentType = .oneTimeCode
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - FirstVC Methods
    
    private func getLasUserEmail () {
        
        passwordTextField.text = ""
        
        if defaults.string(forKey: "lastUserLoggedInEmail") != "" {
            
            userTextField.text = defaults.string(forKey: "lastUserLoggedInEmail")
            
        }
        
    }
    
    
    //MARK: - Actions
    
    @IBAction func loginButtonWasPressed(_ sender: BorderButton) {
        
        if let user = userTextField.text, let password = passwordTextField.text {
            
            Auth.auth().signIn(withEmail: user, password: password) { (authResult, error) in
                
                if let error = error {
                    
                    let errorAlert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                    
                    errorAlert.addAction(action)
                    
                    self.present(errorAlert, animated: true, completion: nil)
                    
                } else {
                    
                    self.defaults.setValue(self.userTextField.text, forKey: "lastUserLoggedInEmail")
                    self.performSegue(withIdentifier: K.SegueIdentifiers.firstToLocalization, sender: self)
                    
                }
            }
            
        }
        
    }
    
    @IBAction func signUpButtonWasPressed(_ sender: BorderButton) {
        
    }
    
    @IBAction func recoverPasswordButtonWasPressed(_ sender: UIButton) {
    }

    //MARK: - FirstVC Methods
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}


//MARK: - UITextFieldDelegateMethods

extension FirstVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    
}

