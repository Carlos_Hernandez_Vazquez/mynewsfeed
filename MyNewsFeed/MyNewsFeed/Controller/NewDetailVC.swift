//
//  NewDetailVC.swift
//  MyNewsFeed
//
//  Created by mac on 11/04/21.
//

import UIKit
import SafariServices

class NewDetailVC: UIViewController {

    //MARK: - Properties
    
    private var url = URL(string: "https://google.com")
    private var hasBeenLoaded : Bool = false
    
    //MARK: - Lyfe Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        
        let viewController = SFSafariViewController(url: self.url!)
        
        viewController.dismissButtonStyle = .done
        viewController.delegate = self
        
        present(viewController, animated: true, completion: nil)
        
        
    }
    
    //MARK: - NewDetailVC Methods
    
    public func getURL (url : String?) {
        
        if let safeURL = url {
            
            if let url = URL(string: safeURL) {
                
                print(url)
                self.url = url
                
                
            }
            
            
        } else {
            
            let alert = UIAlertController(title: "New's URL was not found", message: "", preferredStyle: .alert)
            
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            
            alert.addAction(action)
            
            present(alert, animated: true, completion: nil)
            
            
        }
        
        
    }
    
    private func didFinishLoadingWebView () {
        
        if hasBeenLoaded {
            
            _ = navigationController?.popViewController(animated: true)

            
        }
        
        
    }
    
    
}


extension NewDetailVC : SFSafariViewControllerDelegate {
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        
        hasBeenLoaded = true
        
        didFinishLoadingWebView()
        
        
    }
    
    
}
