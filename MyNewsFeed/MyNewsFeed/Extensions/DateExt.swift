//
//  DateExt.swift
//  MyNewsFeed
//
//  Created by Carlos Hernández Vázquez on 24/03/21.
//

import Foundation

extension Date {
    
    func addformat () -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter.string(from: self)
        
    }
    
    
}
