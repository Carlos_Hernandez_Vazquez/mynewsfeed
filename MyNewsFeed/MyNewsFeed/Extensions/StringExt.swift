//
//  StringExt.swift
//  MyNewsFeed
//
//  Created by Carlos Hernández Vázquez on 25/03/21.
//

import Foundation

extension String {
    
    mutating func showOnlyTime () -> String {
        
        let RFC3339DateFormatter = DateFormatter()
        RFC3339DateFormatter.locale = Locale(identifier: "en_US_POSIX")
        RFC3339DateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        RFC3339DateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        let date = RFC3339DateFormatter.date(from: self)
        RFC3339DateFormatter.dateFormat = "HH:mm"
        
        return RFC3339DateFormatter.string(from: date!)
        
    }
    
}
